#ifndef ARGS_H
#define ARGS_H

#include <stdbool.h>

typedef struct arguments {
    char *endpoint_name;
    char *server_uri;
    char *psk_id;
    char *psk_key;
    bool game_enable;
    int game_magic_number;
} arguments_t;

int parse(int argc, char *argv[], arguments_t *arguments);

#endif
