#ifndef OBJECTS_H
#define OBJECTS_H

#include <anjay/dm.h>

// Device Object - 3 v1.0
const anjay_dm_object_def_t **device_object_create(char **argv);
void device_object_release(const anjay_dm_object_def_t **def);
void device_object_notify(anjay_t *anjay, const anjay_dm_object_def_t **def);

// Connectivity Monitoring Object - 4 v1.0
const anjay_dm_object_def_t **connectivity_monitoring_object_create(void);
void connectivity_monitoring_object_release(const anjay_dm_object_def_t **def);

// Temperature Object - 3303
const anjay_dm_object_def_t **temperature_object_create(void);
void temperature_object_release(const anjay_dm_object_def_t **def);
void temperature_object_notify(anjay_t *anjay, const anjay_dm_object_def_t **def);

// Game Object - 1111
const anjay_dm_object_def_t **game_object_create(int magic_number);
void game_object_release(const anjay_dm_object_def_t **def);

#endif
