#include <argp.h>
#include "args.h"
#include <stdlib.h>

static const struct argp_option options[] = {
        {"endpoint-name", 'n', "NAME", 0, "Endpoint name to use."},
        {"server-uri", 'u', "ADDR", 0,"Server URI to use. For example: coaps://0.0.0.0:1234  "},
        {"psk-id", 'i', "ID", 0,"PSK id"},
        {"psk-key", 'k', "KEY", 0,"PSK key."},
        {"game-enable", 'g', "NUMBER 0-100 INT", OPTION_ARG_OPTIONAL,"Enables game object. If given argument - changes magic number in game"},
        { 0 }   //always needed to terminate with zero
};

static error_t parse_one_option(int key, char *arg, struct argp_state *state) {
    arguments_t *arguments = state->input;

    switch (key) {
    case 'n':
        arguments->endpoint_name = arg;
        break;
    case 'u':
        arguments->server_uri = arg;
        break;
    case 'i':
        arguments->psk_id = arg;
        break;
    case 'k':
        arguments->psk_key = arg;
        break;
    case 'g':
        arguments->game_enable = true;
        if (arg) {
            char *endptr;
            arguments->game_magic_number = (int) strtol(arg, &endptr, 10);
        }
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

int parse(int argc, char *argv[], arguments_t *arguments) {
    static const char doc[] = "LwM2M Client based on Anjay, with psk security mode.";
    static const struct argp argp = {options, parse_one_option, 0, doc};
    return (int) argp_parse( &argp, argc, argv, 0, 0, arguments );
}
