#include <anjay/anjay.h>
#include <anjay/attr_storage.h>
#include <anjay/security.h>
#include <anjay/server.h>
#include <avsystem/commons/avs_log.h>

#include <poll.h>
#include <argp.h>

#include "objects.h"
#include "args.h"

// Code base on Anjay tutorial:
// https://avsystem.github.io/Anjay-doc/BasicClient.html

int main_loop(anjay_t *anjay, const anjay_dm_object_def_t **temperature_object,
                              const anjay_dm_object_def_t **device_object) {
    while (true) {
        // Obtain all network data sources
        AVS_LIST(avs_net_socket_t *const) sockets = anjay_get_sockets(anjay);

        // Prepare to poll() on them
        size_t numsocks = AVS_LIST_SIZE(sockets);
        struct pollfd pollfds[numsocks];
        size_t i = 0;
        AVS_LIST(avs_net_socket_t *const) sock;
        AVS_LIST_FOREACH(sock, sockets) {
            pollfds[i].fd = *(const int *) avs_net_socket_get_system(*sock);
            pollfds[i].events = POLLIN;
            pollfds[i].revents = 0;
            ++i;
        }

        const int max_wait_time_ms = 1000;
        // Determine the expected time to the next job in milliseconds.
        // If there is no job we will wait till something arrives for
        // at most 1 second (i.e. max_wait_time_ms).
        int wait_ms =
                anjay_sched_calculate_wait_time_ms(anjay, max_wait_time_ms);

        // Wait for the events if necessary, and handle them.
        if (poll(pollfds, numsocks, wait_ms) > 0) {
            int socket_id = 0;
            AVS_LIST(avs_net_socket_t *const) socket = NULL;
            AVS_LIST_FOREACH(socket, sockets) {
                if (pollfds[socket_id].revents) {
                    if (anjay_serve(anjay, *socket)) {
                        avs_log(anjayLH, ERROR, "anjay_serve failed");
                    }
                }
                ++socket_id;
            }
        }

        device_object_notify(anjay, device_object);
        temperature_object_notify(anjay, temperature_object);

        // Finally run the scheduler
        // Important to invoke it regularly
        anjay_sched_run(anjay);
    }
    return 0;
}

// Installs Security Object and adds and instance of it.
// Using PSK security mode
static int setup_security_object(anjay_t *anjay, const char *uri,
                                                 const char *key,
                                                 const char *id) {
    if (anjay_security_object_install(anjay)) {
        return -1;
    }

    anjay_security_instance_t security_instance = {
        .ssid = 1,
        .server_uri = uri,
        .security_mode = ANJAY_SECURITY_PSK,
        .public_cert_or_psk_identity = (const uint8_t *) id,
        .public_cert_or_psk_identity_size = strlen(id),
        .private_cert_or_psk_key = (const uint8_t *) key,
        .private_cert_or_psk_key_size = strlen(key)
    };

    // Anjay will assign Instance ID automatically
    anjay_iid_t security_instance_id = ANJAY_ID_INVALID;
    if (anjay_security_object_add_instance(anjay, &security_instance,
                                           &security_instance_id)) {
        return -1;
    }

    return 0;
}

// Installs Server Object and adds a instance of it.
static int setup_server_object(anjay_t *anjay) {
    if (anjay_server_object_install(anjay)) {
        return -1;
    }

    const anjay_server_instance_t server_instance = {
        // Server Short ID
        .ssid = 1,
        // Client will send Update message often than every 60 seconds
        .lifetime = 60,
        // Disable Default Minimum Period resource
        .default_min_period = -1,
        // Disable Default Maximum Period resource
        .default_max_period = -1,
        // Disable Disable Timeout resource
        .disable_timeout = -1,
        // Sets preferred transport to UDP
        .binding = "U"
    };

    // Anjay will assign Instance ID automatically
    anjay_iid_t server_instance_id = ANJAY_ID_INVALID;
    if (anjay_server_object_add_instance(anjay, &server_instance,
                                         &server_instance_id)) {
        return -1;
    }

    return 0;
}

int main(int argc, char *argv[]) {
    static arguments_t default_arguments = {
        .endpoint_name = "ID123456",
        .server_uri = "coaps://try-anjay.avsystem.com:5684",
        .psk_id = "ID123456",
        .psk_key = "AVjest$uper",
        .game_enable = false,
        .game_magic_number = 77
    };

    int parse_result = parse(argc, argv, &default_arguments);
    if (parse_result == ARGP_ERR_UNKNOWN) {
        avs_log(anjayLH, ERROR, "Wrong options.");
        return 0;
    } else if (parse_result != 0) {
        avs_log(anjayLH, ERROR, "Wrong arguments.");
        return 0;
    }

    anjay_configuration_t CONFIG = {
        .endpoint_name = default_arguments.endpoint_name,
        .in_buffer_size = 4000,
        .out_buffer_size = 4000,
        .msg_cache_size = 4000
    };

    anjay_t *anjay = anjay_new(&CONFIG);
    if (!anjay) {
        avs_log(anjayLH, ERROR, "Could not create Anjay object");
        return -1;
    }

    int result = 0;
    // Install Attribute storage and setup necessary objects
    if (anjay_attr_storage_install(anjay)
        || setup_security_object(anjay, default_arguments.server_uri,
                                        default_arguments.psk_key,
                                        default_arguments.psk_id)
        || setup_server_object(anjay)) {
        result = -1;
    }

    const anjay_dm_object_def_t **temperature_object = NULL;
    if (!result) {
        temperature_object = temperature_object_create();
        if (temperature_object) {
            result = anjay_register_object(anjay, temperature_object);
        } else {
            result = -1;
        }
    }

    const anjay_dm_object_def_t **device_object = NULL;
    if (!result) {
        device_object = device_object_create(argv);
        if (device_object) {
            result = anjay_register_object(anjay, device_object);
        } else {
            result = -1;
        }
    }

    const anjay_dm_object_def_t **conn_monitoring_object = NULL;
    if (!result) {
        conn_monitoring_object = connectivity_monitoring_object_create();
        if (conn_monitoring_object) {
            result = anjay_register_object(anjay, conn_monitoring_object);
        } else {
            result = -1;
        }
    }

    const anjay_dm_object_def_t **game_object= NULL;
    if (default_arguments.game_enable) {
        if (!result) {
            game_object = game_object_create(default_arguments.game_magic_number);
            if (game_object) {
                result = anjay_register_object(anjay, game_object);
            } else {
                result = -1;
            }
        }
    }

    if (!result) {
        result = main_loop(anjay, temperature_object, device_object);
    }

    anjay_delete(anjay);
    temperature_object_release(temperature_object);
    device_object_release(device_object);
    connectivity_monitoring_object_release(conn_monitoring_object);
    if (default_arguments.game_enable) {
        game_object_release(game_object);
    }

    return result;
}