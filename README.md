# AnjayLH - LwM2M Ubuntu client

## Project Overview
This project is implementation of OMA Lightweight Machine-to-Machine (LwM2M) Client based on [Anajay](https://github.com/AVSystem/Anjay) library. Project targets x86-64 PC with Ubuntu 20.04 LTS as a operating system.

There are following LwM2M objects supported:
- **LwM2M Security (/0)**: 
    - PSK security mode
- **LwM2M Server (/1)** 
- **Device (/3)**:
    - allows reading real values of total and free (with notifications) device memory
    - allows reboot of application, with initial arguments
- **Connectivity Monitoring (/3)**:
    - gives real IP address used by device to communicate with Internet
- **Temperature (/3303)**:
    - used for reading real CPU temperature (with notifications)
- **Game (/1111 - custom)**:
    - read-only object with executable resource (rid=0 string R, rid=1 E, rid=2 string R)
    - user executes resource with int argument and after refreshing one of readable resourse, get inforamtion if his value was 'magic number'
    - enable by giving '-g' application argument
    
Application supports easy building using CMake/make and accepts arguments which can modify some parameters - described below.
## Compilation Guide
First you need to install Anjay library (in /usr/local) and other needed dependencies:
 ```
sudo apt-get install git build-essential cmake libmbedtls-dev zlib1g-dev
git clone https://github.com/AVSystem/Anjay.git 
cd Anjay
git submodule update --init
cmake . && make && sudo make install
 ```
Then go to directory dedicated for application, paste lukasz_hajec.bundle there and:
 ```
git clone lukasz_hajec.bundle
cd lukasz_hajec
cmake . && make
 ```
 
## Running application

You can run application with default parameters (to find at the beginning of [main.c](src/main.c)):
 ```
./anjayLH
 ```
Also you can configure some parameters by giving arguments: 
 ```
./anjayLH -n ID000000 -u coaps://127.0.0.1 --psk-id ID000000 -g10 
# -n configures endpoint_name, -u server uri addr, -g10 enables game object and sets 'magic number' to 10
 ```
You can access some other basic importations of application usage by typing:
 ```
./anjayLH --help
./anjayLH --usage
 ```
