#include <anjay/anjay.h>
#include <avsystem/commons/avs_defs.h>
#include <avsystem/commons/avs_list.h>
#include <avsystem/commons/avs_memory.h>

// Code based on Anjay tutorial:
// https://avsystem.github.io/Anjay-doc/AdvancedTopics/AT-CustomObjects/AT_CO2_SingleInstanceExecutableAndReadOnly.html

typedef struct game_object_struct {
    const anjay_dm_object_def_t *def;
    char compare_result[32];
    int magic_number;
} game_object_t;

static inline game_object_t *
get_obj(const anjay_dm_object_def_t *const *obj_ptr) {
    assert(obj_ptr);
    return AVS_CONTAINER_OF(obj_ptr, game_object_t , def);
}

static int game_list_resources(anjay_t *anjay,
                               const anjay_dm_object_def_t *const *obj_ptr,
                               anjay_iid_t iid,
                               anjay_dm_resource_list_ctx_t *ctx) {

    anjay_dm_emit_res(ctx, 0, ANJAY_DM_RES_R, ANJAY_DM_RES_PRESENT);
    anjay_dm_emit_res(ctx, 1, ANJAY_DM_RES_E, ANJAY_DM_RES_PRESENT);
    anjay_dm_emit_res(ctx, 2, ANJAY_DM_RES_R, ANJAY_DM_RES_PRESENT);
    return 0;
}

static int game_resource_read(anjay_t *anjay,
                              const anjay_dm_object_def_t *const *obj_ptr,
                              anjay_iid_t iid,
                              anjay_rid_t rid,
                              anjay_riid_t riid,
                              anjay_output_ctx_t *ctx) {
    (void) anjay;   // unused

    game_object_t *obj = get_obj(obj_ptr);
    assert(obj);

    switch (rid) {
        case 0:
            return anjay_ret_string(ctx, "Guess the magic number, execute with: 0='<0-99>'");
        case 1:
            return ANJAY_ERR_METHOD_NOT_ALLOWED;
        case 2:
            return anjay_ret_string(ctx, obj->compare_result);
        default:
            return 0;
    }
}

static int get_arg_value(anjay_execute_ctx_t *ctx, int *out_value) {
    // we expect argument of form <0-9>='<integer>'
    int arg_number = 0;
    bool has_value;
    int result = anjay_execute_get_next_arg(ctx, &arg_number, &has_value);

    (void) arg_number;

    if (result < 0 || result == ANJAY_EXECUTE_GET_ARG_END) {
        // an error occured or there is just nothing more to read
        return result;
    }

    if (!has_value) {
        // we expect argument with value only
        return ANJAY_ERR_BAD_REQUEST;
    }

    char value_buffer[10];
    if (anjay_execute_get_arg_value(ctx, NULL, value_buffer, sizeof(value_buffer)) != 0) {
        // the value must have been malformed or it is too long - either way, we don't like it
        return ANJAY_ERR_BAD_REQUEST;
    }
    char *endptr = NULL;
    long value = strtol(value_buffer, &endptr, 10);
    if (!endptr || *endptr != '\0' || value < INT_MIN || value > INT_MAX) {
        // either not an integer or the number is too small / too big
        return ANJAY_ERR_BAD_REQUEST;
    }
    *out_value = (int) value;
    return 0;
}

static int game_resource_execute(anjay_t *anjay,
                                 const anjay_dm_object_def_t *const *obj_ptr,
                                 anjay_iid_t iid,
                                 anjay_rid_t rid,
                                 anjay_execute_ctx_t *ctx) {
    game_object_t *obj = get_obj(obj_ptr);

    switch (rid) {
        case 1: {
            int result;
            int arg_value = 0;
            // We  expect only one value.
            result = get_arg_value(ctx, &arg_value);

            // No need for returning non zero value, better to inform user in readable resource.
            if (result != 0) {
                strcpy(obj->compare_result, "You need to give proper argument!");
            } else {
                if (arg_value == obj->magic_number) {
                    strcpy(obj->compare_result, "Great! You won :D");
                } else if (arg_value > obj->magic_number - 10 && arg_value < obj->magic_number + 10) {
                    strcpy(obj->compare_result, "You were close!");
                } else {
                    strcpy(obj->compare_result, "Let's try again ;)");
                }
            }

            anjay_notify_changed(anjay, 1111, iid, rid);

            return 0;
        }
        default:
            // no other resource is executable
            return ANJAY_ERR_METHOD_NOT_ALLOWED;
    }
}

static const anjay_dm_object_def_t OBJ_DEF = {
        .oid = 1111,

        .handlers = {
                // single-instance Objects can use this pre-implemented handler:
                .list_instances = anjay_dm_list_instances_SINGLE,

                .list_resources = game_list_resources,
                .resource_read = game_resource_read,
                .resource_execute = game_resource_execute
        }
};

const anjay_dm_object_def_t **game_object_create(int magic_number) {
    game_object_t *obj = (game_object_t *) avs_calloc(1, sizeof(game_object_t));
    if (!obj) {
        return NULL;
    }
    obj->def = &OBJ_DEF;

    //Rule is that magic number is from 0 to 100 int.
    if (magic_number < 0 || magic_number > 100) {
        obj->magic_number = 50;
    } else {
        obj->magic_number = magic_number;
    }
    strcpy(obj->compare_result, "Give input and execute!");

    return &obj->def;
}

void game_object_release(const anjay_dm_object_def_t **def) {
    if (def) {
        game_object_t *obj = get_obj(def);
        avs_free(obj);
    }
}




